DROP TABLE IF EXISTS places;

CREATE TABLE places (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    visited BOOLEAN
);

INSERT INTO places (name, visited) 
VALUES 
    ('Tokyo', false),
    ('Budapest', true),
    ('Nairobi', false),
    ('Berlin', true),
    ('Lisbon', true),
    ('Denver', false),
    ('Moscow', false),
    ('Olso', false),
    ('Rio', true),
    ('Cincinnati', false),
    ('Helsinki', false);
